import { RecipeService } from '../recipe-service/recipe-service.service';
import { Component, EventEmitter, Output, OnInit} from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'rb-recipe-list',
  templateUrl: './recipe-list.component.html'
})
export class RecipeListComponent implements OnInit{

  constructor(private recipeService: RecipeService) {}

  recipes: Recipe[] = [];

  @Output() recipeSelected = new EventEmitter<Recipe>();

  onSelected(recipeIn: Recipe){
      this.recipeSelected.emit(recipeIn);
  }

  ngOnInit(){
      this.recipes = this.recipeService.getRecipes();
  }

}
