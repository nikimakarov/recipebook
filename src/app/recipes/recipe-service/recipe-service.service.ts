import { Injectable } from '@angular/core';
import { Recipe } from '../recipe';
@Injectable()
export class RecipeService {

  private recipes: Recipe[] = [
      new Recipe('Cheese Cake', 'This cake is easy to make, and it is so delicious!', 'http://bit.ly/2fdofHX',[]),
      new Recipe('Chocolate Cake', 'Chocolate cake that melts in your mouth for the best chocolate delight every time.', 'http://bit.ly/251W7Mm',[])
      ];

  constructor() { }

  getRecipes(){
      return this.recipes;
  }
}
