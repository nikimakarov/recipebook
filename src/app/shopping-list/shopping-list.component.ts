import { Component } from '@angular/core';
import { Ingredient } from '../shared/ingredient';
@Component({
  selector: 'rb-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent{
    items: Ingredient[] = [];
}
